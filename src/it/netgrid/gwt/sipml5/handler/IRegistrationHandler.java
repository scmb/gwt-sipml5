package it.netgrid.gwt.sipml5.handler;

import it.netgrid.gwt.sipml5.event.RegistrationEvent;

public interface IRegistrationHandler extends IEventHandler<RegistrationEvent> {

}
