package it.netgrid.gwt.sipml5.handler;

import it.netgrid.gwt.sipml5.event.MessageEvent;

public interface IMessageEventHandler extends IEventHandler<MessageEvent> {

}
