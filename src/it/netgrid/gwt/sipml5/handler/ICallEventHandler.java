package it.netgrid.gwt.sipml5.handler;

import it.netgrid.gwt.sipml5.event.CallEvent;

public interface ICallEventHandler extends IEventHandler<CallEvent> {

}
