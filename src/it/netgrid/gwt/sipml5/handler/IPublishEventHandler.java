package it.netgrid.gwt.sipml5.handler;

import it.netgrid.gwt.sipml5.event.PublishEvent;

public interface IPublishEventHandler extends IEventHandler<PublishEvent> {

}
