package it.netgrid.gwt.sipml5.handler;

import it.netgrid.gwt.sipml5.event.StackEvent;

public interface IStackEventHandler extends IEventHandler<StackEvent> {

}
