package it.netgrid.gwt.sipml5.handler;

import it.netgrid.gwt.sipml5.event.SubscribeEvent;

public interface ISubscribeHandler extends IEventHandler<SubscribeEvent> {

}
